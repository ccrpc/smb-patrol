# SMB Patrol

Automated file structure enforcement for Samba shares

## Credits
SMB Patrol was developed by Matt Yoder for the [Champaign County
Regional Planning Commission][1].

## License
SMB Patrol is available under the terms of the [BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/smb-patrol/blob/master/LICENSE.md
