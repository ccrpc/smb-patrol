FROM python:3-alpine

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code

ADD requirements.txt /code/
RUN apk add --no-cache --virtual .build-deps \
    build-base \
    openldap-dev \
    python2-dev \
    python3-dev && \
  pip install --no-cache-dir -r requirements.txt && \
  apk del --no-cache .build-deps && \
  apk add --no-cache openldap
ADD smb-patrol /code/smb-patrol
CMD python -m smb-patrol /etc/config.json
