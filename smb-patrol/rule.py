import re
from .issue import Issue


class Rule:
    def __init__(self, share, config):
        self.share = share
        self.pattern = re.compile(config['pattern'])
        self.warning = config['warning']
        self.action = config['action']

    def issues(self, paths):
        for path in paths:
            if self.pattern.match(path):
                yield Issue(self, path)
