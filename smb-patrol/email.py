from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class BaseEmail:

    def __init__(self, subject, sender, recipients, cc_recipients=[]):
        self.subject = subject
        self.sender = sender
        self.recipients = recipients
        self.cc_recipients = cc_recipients

    def text_body(self):
        return ''

    def html_body(self):
        return ''

    def message(self):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = self.subject
        msg['From'] = self.sender
        msg['To'] = ','.join(self.recipients)
        if self.cc_recipients:
            msg['CC'] = ','.join(self.cc_recipients)

        text = '\n\n'.join([self.subject.upper(), self.text_body()])

        body_style = 'style="font-family: arial, sans-serif;"'
        html = f'<html><head></head><body {body_style}><h1>{self.subject}</h1>'
        html += f'{self.html_body()}</body></html>'

        msg.attach(MIMEText(text, 'plain'))
        msg.attach(MIMEText(html, 'html'))
        return msg


class UserEmail(BaseEmail):

    def __init__(self, smtp_config, user):
        self.smtp_config = smtp_config
        self.issues = user.issues

        plural = 's' if len(user.issues) > 1 else ''
        subject = smtp_config['subject'] % (user.name,)
        subject += f': {len(user.issues)} issue{plural}'

        aliases = smtp_config.get('aliases', {}).get(user.email, {})
        recipients = aliases.get('to', [user.email])
        cc_recipients = aliases.get('cc', [])

        super().__init__(
            subject, smtp_config['from'], recipients, cc_recipients)

    def text_body(self):
        text = self.smtp_config.get('text', '')
        for issue in self.issues:
            text += f'\n\n{issue.windows_path()}'
            text += f'\nWarning: {issue.rule.warning}'
            text += f'\nAction: {issue.rule.action}'

        return text

    def html_body(self):
        intro = self.smtp_config.get('html', '')
        html = f'<p>{intro}</p>' if intro else ''
        html += '<ol>'

        for issue in self.issues:
            html += f'<li><strong>{issue.windows_path()}</strong><br />'
            html += f'<strong>Warning:</strong> {issue.rule.warning}<br />'
            html += f'<strong>Action:</strong> {issue.rule.action}<br />'
            html += f'<a href="{issue.parent_url()}">'
            html += 'Open containing folder</a><br />'
            html += '<br /></li>'

        html += '</ol>'
        return html


class SummaryEmail(BaseEmail):

    def __init__(self, smtp_config, users, admin_email):
        self.smtp_config = smtp_config
        self.users = users
        subject = smtp_config['subject'] % ('administrator',)
        super().__init__(subject, smtp_config['from'], [admin_email],)

    def text_body(self):
        return '\n'.join([
            f'{u.name}: {len(u.issues)} issues' for u in self.users])

    def html_body(self):
        html = '<ul>'
        for user in self.users:
            html += f'<li><strong>{user.name}:</strong> '
            html += f'{len(user.issues)} issues</li>'

        html += '</ul>'
        return html
