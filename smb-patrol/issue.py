

class Issue:
    def __init__(self, rule, path):
        self.rule = rule
        self.path = path

    def parent_url(self):
        return (
            f'file://\\\\{self.rule.share.client.remote_name}' +
            f'\\{self.rule.share.name}' +
            '\\'.join(self.path.rstrip('/').split('/')[:-1])
        )

    def windows_path(self):
        return self.rule.share.path + self.path.rstrip('/').replace('/', '\\')

    def row(self):
        return [
            self.windows_path(),
            self.rule.warning,
            self.rule.action
        ]
