import os
import re
from .rule import Rule


class Share:
    def __init__(self, client, config):
        self.client = client
        self.name = config['name']
        self.path = config['path']
        self.filters = [re.compile(f) for f in config.get('filters', [])]
        self.rules = [Rule(self, r) for r in config.get('rules', [])]

    def children(self, path):
        files = self.client.listPath(self.name, path)
        return [os.path.join(path, f.filename) + ('/' if f.isDirectory else '')
                for f in files]

    def filter(self, paths):
        paths = [p for p in paths if p not in ['.', '..']]
        for pattern in self.filters:
            paths = [p for p in paths if not pattern.match(p)]

        return paths

    def issues(self, path='/'):
        issues = []
        paths = self.filter(self.children(path))

        for rule in self.rules:
            issues.extend(rule.issues(paths))

        for path in paths:
            if path.endswith('/'):
                issues.extend(self.issues(path))

        return issues
