import csv
import smtplib
import ldap
from datetime import date
from smb.SMBConnection import SMBConnection
from .email import UserEmail, SummaryEmail
from .share import Share
from .user import User


class Patrol:
    def __init__(self, config):
        self.config = config

    def _get_issues(self, smb_client):
        issues = []
        for share_config in self.config.get('shares', []):
            share = Share(smb_client, share_config)
            issues.extend(share.issues())

        return issues

    def _get_users(self, smb_client, issues):
        ldap_config = self.config['ldap']
        ldap_client = ldap.initialize(f'ldap://{ldap_config["ip"]}')
        ldap_client.protocol_version = ldap.VERSION3
        ldap_client.set_option(ldap.OPT_REFERRALS, 0)
        ldap_client.simple_bind_s(ldap_config['user'], ldap_config['password'])

        users = {}
        users['unknown'] = User({
            'displayName': [b'Unknown'],
            'mail': [bytes(self.config['admin'], 'utf-8')]
        })
        attributes = ['displayName', 'mail']

        for issue in issues:
            security = smb_client.getSecurity(
                issue.rule.share.name, issue.path)
            sid = str(security.owner)

            if sid not in users:
                criteria = f'(&(objectClass=user)(objectSID={sid}))'
                results = ldap_client.search_s(
                    ldap_config['base'], ldap.SCOPE_SUBTREE,
                    criteria, attributes)

                for dn, entry in results:
                    if isinstance(entry, dict):
                        users[sid] = User(entry)
                        break

            user = users.get(sid, users['unknown'])
            user.issues.append(issue)

        ldap_client.unbind()
        return sorted(users.values())

    def _write_report(self, users):
        date_string = date.today().strftime('%Y%m%d')
        path = self.config['report'] % (date_string)

        rows = []
        for user in users:
            rows.extend([[user.name] + issue.row() for issue in user.issues])

        with open(path, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['User', 'Path', 'Warning', 'Action'])
            if rows:
                writer.writerows(sorted(rows))

    def _send_emails(self, emails):
        smtp_config = self.config['smtp']

        for email in emails:
            if smtp_config.get('debug', False) or email.recipients == ['']:
                email.recipients = [self.config['admin']]
                email.cc_recipients = []

        smtp = smtplib.SMTP(smtp_config['host'], smtp_config['port'])
        smtp.ehlo()
        smtp.starttls()
        smtp.login(smtp_config['user'], smtp_config['password'])

        for email in emails:
            recipients = email.recipients + email.cc_recipients
            smtp.sendmail(
                email.sender, recipients, email.message().as_string())

        smtp.quit()

    def _get_user_emails(self, users):
        return [UserEmail(self.config['smtp'], user)
                for user in users if user.issues]

    def _get_summary_email(self, users):
        return SummaryEmail(self.config['smtp'], users, self.config['admin'])

    def run(self, report=True, email=True, summary=True):
        smb_config = self.config['smb']
        smb_client = SMBConnection(
            smb_config['user'], smb_config['password'],
            smb_config['client'], smb_config['server'],
            use_ntlm_v2=True)
        smb_client.connect(smb_config['ip'])

        issues = self._get_issues(smb_client)
        users = self._get_users(smb_client, issues)

        if report:
            self._write_report(users)

        emails = []
        if email:
            emails.extend(self._get_user_emails(users))
        if summary:
            emails.append(self._get_summary_email(users))

        if emails:
            self._send_emails(emails)
