from functools import total_ordering


@total_ordering
class User:
    def __init__(self, entry={}):
        names = entry.get('displayName', [])
        emails = entry.get('mail', [])

        self.name = names[0].decode('utf-8') if names else ''
        self.email = emails[0].decode('utf-8') if emails else ''
        self.issues = []

    def __repr__(self):
        return f'<User: {self.name} ({len(self.issues)})>'

    def __eq__(self, other):
        return (self.name, self.email) == (other.name, other.email)

    def __lt__(self, other):
        return (self.name, self.email) < (other.name, other.email)
