import json
import sys
from .patrol import Patrol


with open(sys.argv[1]) as json_file:
    config = json.load(json_file)

patrol = Patrol(config)
patrol.run()
